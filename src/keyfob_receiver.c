// LED Glow [19-20 ID bits][5-4 data bits][1 stop bit]
// TNG [23 serial bits][1 LVD][4 data][4 check sum]
#include "system.h"

// states for receiver
#define KEYFOB_IDLE 0
#define KEYFOB_SERIAL 1
#define KEYFOB_INTERMEDIATE 2
#define KEYFOB_DATA 3
#define KEYFOB_POST_DATA 4

#ifndef KEYFOB_CONSECUTIVE_SERIALS
#define KEYFOB_CONSECUTIVE_SERIALS 4  /// if system.h doesn't define this
#endif
// structure to hold saved serial numbers, callback pointer, and learning mode 
// as well as to buffer all data received
// Note: corruption will occur if Keyfob_Process is not ran by the task management
// module before another entire message is received. This should not be an issue
// as the messages are about 30-40ms long
struct {
    uint32_t serial;
    uint32_t saved_serials[KEYFOB_NUM_REMOTES];
    uint8_t data;
    uint8_t intermediate;
    uint8_t post_data;
    uint8_t learning;
    void (*callback)(uint8_t);
} keyfob;

void Keyfob_LearnRemote(uint32_t serial);

void Keyfob_Init(void (*callback)(uint8_t)) {
    uint8_t i;
    union32_t u32;
    // set up input compare module for or other hal modules
    hal_Keyfob_Init();
    
    // load learned serial numbers
    for(i = 0; i < KEYFOB_NUM_REMOTES; i++) {
        u32.word[0] = EEPROM_Read(KEYFOB_EE_BASE + (i<<1));
        u32.word[1] = EEPROM_Read(KEYFOB_EE_BASE + 1 + (i<<1));
        keyfob.saved_serials[i] = u32.phrase;
    }

    keyfob.learning = 0;
    keyfob.callback = callback;
}

void Keyfob_Process(void) {
    static uint16_t count = 0;
    static uint8_t last_data;
    static uint32_t last_serial;
    // check if we have a match of serial numbers
    uint8_t i;
    for(i = 0; i < KEYFOB_NUM_REMOTES; i++) {
        if(keyfob.saved_serials[i] == keyfob.serial) break;
    }
    if(i == KEYFOB_NUM_REMOTES) {
        if(keyfob.learning) Keyfob_LearnRemote(keyfob.serial);
        return;
    }
    if(keyfob.serial == last_serial && keyfob.data == last_data) {
        count++;
    }else {
        count = 0;
        last_serial = keyfob.serial;
        last_data = keyfob.data;
    }
    if(count < KEYFOB_CONSECUTIVE_DATA) return;
    if(keyfob.callback) keyfob.callback(keyfob.data);
}


void Keyfob_LearnRemote(uint32_t serial) {
    // index to use if all EEPROM slots are full
    static uint8_t index = 0;
    static uint8_t count = 0;
    static uint32_t last_serial = 0;
    union32_t u32;
    uint8_t i;
    if(serial == last_serial) count++;
    else {
        count = 0;
        last_serial = serial;
    }
    if(count < KEYFOB_CONSECUTIVE_SERIALS) return;
    
    // check if any EEPROM slots are "unused"
    for(i = 0; i < KEYFOB_NUM_REMOTES; i++) {
        u32.word[0] = EEPROM_Read(KEYFOB_EE_BASE + (i<<1));
        if(u32.word[0] == 0 || u32.word[0] == 0xFFFF) break;
    }
    if(i < KEYFOB_NUM_REMOTES) index = i;
    u32.phrase = serial;
    EEPROM_Write(KEYFOB_EE_BASE + (index<<1), u32.word[0]);
    EEPROM_Write(KEYFOB_EE_BASE + 1 + (index<<1), u32.word[1]);
    keyfob.saved_serials[index] = serial;
    index = (index == KEYFOB_NUM_REMOTES-1) ? 0 : index+1;
}

void Keyfob_EnableLearning(void) {
    keyfob.learning = 1;
}

void Keyfob_DisableLearning(void) {
    keyfob.learning = 0;
}

void Keyfob_ProcessPulse(uint16_t pulse) {
    static uint8_t n = 0;
    static uint8_t bit_state = 0;
    static uint16_t time_high, time_low;
    static uint32_t serial;
    static uint8_t intermediate, data, post_data;
    static uint8_t state = KEYFOB_IDLE;
    
    if(pulse < KEYFOB_MIN_TIME_UNIT) return;
    switch(state) {
        case KEYFOB_IDLE:
            if(pulse < (KEYFOB_MIN_TIME_UNIT * KEYFOB_PREAMBLE_UNITS)) return;
            else if(pulse < (KEYFOB_MAX_TIME_UNIT * KEYFOB_PREAMBLE_UNITS)) {
                state = KEYFOB_SERIAL;
                n = KEYFOB_SERIAL_BITS;
                bit_state = 0;
                serial = 0;
            }
            break;
        case KEYFOB_SERIAL:
            // if the bit is too long
            if(pulse > KEYFOB_MAX_TIME_UNIT * 3) {
                state = KEYFOB_IDLE;
                return;
            }
            // toggle bit state
            bit_state ^= 1;
            if(bit_state) time_high = pulse;
            else time_low = pulse;
            // if we received a high and a low
            if(bit_state == 0) {
                serial <<= 1;
                if(time_high > KEYFOB_MAX_TIME_UNIT && time_low < KEYFOB_MAX_TIME_UNIT) {
                    serial |= 1;
                }
                n--;
                if(n == 0) {
#if KEYFOB_INTERMEDIATE_BITS > 0
                    state = KEYFOB_INTERMEDIATE;
                    n = KEYFOB_INTERMEDIATE_BITS;
                    intermediate = 0;
#else
                    state = KEYFOB_DATA;
                    n = KEYFOB_DATA_BITS;
                    data = 0;
#endif
                }
            }
            break;
#if KEYFOB_INTERMEDIATE_BITS > 0
        case KEYFOB_INTERMEDIATE:
            fill this in
            break;
#endif
        case KEYFOB_DATA:
            // if the bit is too long
            if(pulse > KEYFOB_MAX_TIME_UNIT * 3) {
                state = KEYFOB_IDLE;
                return;
            }
            // toggle bit state
            bit_state ^= 1;
            if(bit_state) time_high = pulse;
            else time_low = pulse;
            // if we received a high and a low
            if(bit_state == 0) {
                data <<= 1;
                if(time_high > KEYFOB_MAX_TIME_UNIT && time_low < KEYFOB_MAX_TIME_UNIT) {
                    data |= 1;
                }
                n--;
                if(n == 0) {
#if KEYFOB_POST_DATA_BITS > 0
                    state = KEYFOB_POST_DATA;
                    n = KEYFOB_POST_DATA_BITS;
                    post_data = 0;
#else
                    state = KEYFOB_IDLE;
                    keyfob.serial = serial;
                    keyfob.intermediate = intermediate;
                    keyfob.data = data;
                    keyfob.post_data = post_data;
                    Task_Queue(Keyfob_Process, 0);
#endif
                }
            }
            break;
#if KEYFOB_POST_DATA_BITS > 0
        case KEYFOB_POST_DATA:
            fill this in
            break;
#endif
    }
}
