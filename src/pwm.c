/*
 * pwm.c
 *
 *  Created on: Apr 15, 2015
 *      Author: Josh
 */

#include "hal_pwm.h"
#include "pwm.h"

void pwm_init(void) {
	hal_pwm_init();
}

void pwm_setPeriod(uint16_t period){
	hal_pwm_setPeriod(period);
}

void pwm_setDuty(uint16_t duty){
	hal_pwm_setDuty(duty);
}

