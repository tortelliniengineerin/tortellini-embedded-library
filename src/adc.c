/*
 * adc.c
 *
 *  Created on: Apr 9, 2015
 *      Author: Josh
 */

#include <msp430.h>
#include <stdint.h>
#include "adc.h"
#include "timing.h"
#include "hal_adc.h"

//Running a periodic Task to poll the ADC
#define ENABLE_PERIODIC_TASK 0

//The ADC request will be made every given period of ms
#ifndef ADC_REQUEST_PERIOD
#define ADC_REQUEST_PERIOD 5
#endif


//How big is our moving average filter?
#ifndef ADC_MAF_SIZE
#define ADC_MAF_SIZE 5
#endif

uint16_t filter_input[ADC_MAF_SIZE];
char filter_input_ptr = 0;
uint16_t filtered_output = 0;

void adc_init(void) {
	hal_adc_init();

#if ENABLE_PERIODIC_TASK == 1
	Task_Schedule(hal_adc_request_if_possible, 0, 0, ADC_REQUEST_PERIOD);
#endif

	int i = 0;
	for(i=0;i<ADC_MAF_SIZE;i++) {
		filter_input[i] = 0;
	}
}

uint16_t adc_read(void) {
	return hal_adc_read();
}

char adc_new_available(void) {
	return hal_adc_new_available();
}

/*
 * y(n) = y(n-1) + x(n)/N - x(n-N)/N
 */
void adc_update(void) {
	uint16_t read = hal_adc_read();
	filtered_output = filtered_output + (-filter_input[(filter_input_ptr+1)%ADC_MAF_SIZE] + read)/ADC_MAF_SIZE;
	filter_input[filter_input_ptr%ADC_MAF_SIZE] = read;
	filter_input_ptr = (filter_input_ptr+1)%ADC_MAF_SIZE;
}
