/** 
 * File:   midi_ble.c
 * Author: r.o.c.k.e.t.
 * Author: Andrew D.
 * Author: Mike Mc.
 * Created on March 28, 2016, 1:29 PM
 */
#include "midi_ble.h"
#include "bluefruit.h"

void MIDI_BLE_Send(uint8_t data[], uint8_t length){
    uint16_t timestamp = TimeNow();
    
    uint8_t header_byte = (timestamp >> 7) & 0b00111111; // get bits 12-7 of timestamp and truncate to 8 bits
    header_byte |= 0b10000000; // append start bit and reserved bit front of timestamp via mask, result: [header_byte] = [start_bit][reserved_bit][timestamp(12:7)]
    
    uint8_t timestamp_byte = timestamp & 0b01111111; // truncates to lower byte
    timestamp_byte |= 0b10000000; // add start byte to front via mask, result: [timestamp_byte] = [start_bit][timestamp(6:0)]
    
    uint8_t packet[20];
    uint8_t packet_str[50];
    
    data[0] = data[0]+0x80;

    packet[0] = header_byte;
    packet[1] = timestamp_byte;
    memcpy(&packet[2], data, length);
    packet[length+2] = 0;
    
    // Convert to byte array format XX-XX-XX-XX-XX
    uint8_t packet_index = 0;
    uint8_t packet_upper;
    uint8_t packet_lower;
    while(packet[packet_index]!=0 || packet_index < 5){ //@todo Ends early if data is 0. Change to for loop with length.
    	if(packet_index) packet_str[(packet_index-1)*3+2] = '-';
    	packet_upper = (packet[packet_index] & 0xF0) >> 4;
    	packet_str[packet_index*3] = int2hex(packet_upper);
    	packet_lower = packet[packet_index] & 0x0F;
    	packet_str[packet_index*3+1] = int2hex(packet_lower);
    	packet_str[packet_index*3+2] = 0;
    	packet_index++;
    }

    bluefruit_GATTWriteChar(1, packet_str);
}

void DelayedMIDI_Init(void){
	MIDI_Init(MIDI_BLE_Send);
}

void MIDI_BLE_Init(void){
	//Wait 4 seconds after initialization before sending midi messages.
	//@todo Messages can be sent before init is completed. Add init flag.
	UART_Init(BLUEFRUIT_CHANNEL);
    bluefruit_Init();
    Task_Schedule(bluefruit_FactoryReset, 0, 50, 0);
    Task_Schedule(bluefruit_GATTClear, 0, 1000, 0);
    Task_Schedule(DelayedGATTAddService128, 0, 1500, 0);
    Task_Schedule(DelayedGATTAddChar128, 0, 2000, 0);
    Task_Schedule(DelayedGAPSetAdvData, 0, 2500, 0);
    Task_Schedule(bluefruit_ATZ, 0, 3000, 0);
    Task_Schedule(DelayedMIDI_Init, 0, 4000, 0);
}

void MIDI_BLE_Receive(uint8_t data){
    
}//Ignore for now

void DelayedGATTAddService128(void){
	char * uuid = "03-B8-0E-5A-ED-E8-4B-33-A7-51-6C-E3-4E-C4-C7-00";
	bluefruit_GATTAddService128(uuid);
}

void DelayedGATTAddChar128(void){
	char * tx_uuid = "77-72-E5-DB-38-68-41-12-A1-A9-F2-66-9D-10-6B-F3";
	bluefruit_GATTAddChar128(tx_uuid, "0x1A", 5, 5, "00000");
}

void DelayedGAPSetAdvData(void){
	char * adv_data = "11-07-03-B8-0E-5A-ED-E8-4B-33-A7-51-6C-E3-4E-C4-C7-00";
	bluefruit_GAPSetAdvData(adv_data);
}

static char int2hex(uint8_t data){
	switch(data){
	case 0:
		return '0';
	case 1:
		return '1';
	case 2:
		return '2';
	case 3:
		return '3';
	case 4:
		return '4';
	case 5:
		return '5';
	case 6:
		return '6';
	case 7:
		return '7';
	case 8:
		return '8';
	case 9:
		return '9';
	case 10:
		return 'A';
	case 11:
		return 'B';
	case 12:
		return 'C';
	case 13:
		return 'D';
	case 14:
		return 'E';
	case 15:
		return 'F';
	}
	return 0;
}
