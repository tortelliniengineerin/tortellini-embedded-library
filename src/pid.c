#include "system.h"
#include "pid.h"

void PID_Reset(pid_controller_t *pid_handle){
	pid_handle->prev_time = TimeNow();
	pid_handle->prev_i = 0;
	pid_handle->prev_error = 0;
}

int32_t PID_Step(pid_controller_t *pid_handle, int32_t meas) {

	// Calculate error
	int32_t curr_error = pid_handle->setpoint - meas;

	// Find new dt
	tint_t dt = (TimeSince(pid_handle->prev_time));

	// Calculate Proportional Term
	int32_t P = pid_handle->kp*curr_error;

	// Calculate Integral Term
	int32_t I = pid_handle->prev_i + (pid_handle->ki*curr_error*dt);
	if(pid_handle->max_i){
		if(I > pid_handle->max_i){
			I = pid_handle->max_i;
		} else if(I < -1*pid_handle->max_i) {
			I = -1*pid_handle->max_i;
		}
	}
	
	int32_t D;
	// Calculate Derivative Term
	if(dt != 0){
		D = (curr_error - pid_handle->prev_error);
		D *= pid_handle->kd;
		D /= (int32_t)dt;
	} else {
		D = 0;
	}

	// Update struct
	pid_handle->prev_i = I;
	pid_handle->prev_error = curr_error;
	pid_handle->prev_time = TimeNow();

	return (P+I/1000+D);
}
