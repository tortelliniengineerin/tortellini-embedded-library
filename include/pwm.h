/*
 * pwm.h
 *
 *  Created on: Apr 15, 2015
 *      Author: Josh
 */

#ifndef PWM_H_
#define PWM_H_

#include <stdint.h>

void pwm_init(void);

void pwm_setDuty(uint16_t duty);

void pwm_setPeriod(uint16_t period);

#endif /* PWM_H_ */
