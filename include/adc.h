/*
 * adc.h
 *
 *  Created on: Apr 9, 2015
 *      Author: Josh
 */

#include <stdint.h>

#ifndef ADC_H_
#define ADC_H_

/**
 * Initializes the HAL ADC system. With a directive enabled, it will also start a looping system to pull
 *   values from the ADC
 */
void adc_init(void);

/**
 * Gets the most recent value from the ADC
 * @return ADC value
 */
uint16_t adc_read(void);

/**
 * As values are read asynchronously, it's possible that you'll read the same value twice.
 *   In order to check every value you pull is a new value, you can use this function.
 *
 * Every the system polls the ADC, a new id is generated. Whenever you read a value, the
 *   read id is caught up. When there's discrepancy between the two, this function returns the
 *   difference. If the two ids are the same, this function returns 0
 *
 * @return the difference between the poll event and the read event
 */
char adc_new_available(void);
/*
 * When a new value is found, through an interrupt or by checking `adc_new_available`,
 *   call this method. It will automatically add to the moving average filter.
 *
 * Note: This function needs to be called every time the adc changes in order to accurately
 *   maintain the filter
 */
void adc_update(void);
#endif /* ADC_H_ */
