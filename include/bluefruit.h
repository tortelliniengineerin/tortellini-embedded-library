/**
 * File:   bluefruit.h
 *
 * @author Christopher Frederickson
 *
 * @defgroup bluefruit Adafruit Bluefrut Interface
 * @ingroup midi
 * @{
 */


#ifndef _BLUEFRUIT_H_
#define	_BLUEFRUIT_H_

#include "system.h"
#include "uart.h"
#include <stdint.h>

#ifdef	__cplusplus
extern "C" {
#endif

#ifndef BLUEFRUIT_CHANNEL
#define BLUEFRUIT_CHANNEL 0
#endif

/**
 * Initializes the bluefruit module
 */
void bluefruit_Init(void);

/**
 * Switches the mode of the bluefruit module
 */
void bluefruit_SwitchMode(void);

/**
 * Ping the bluefruit module
 */
void bluefruit_AT(void);

/**
 * Restart the bluefruit module
 */
void bluefruit_ATZ(void);

/**
 * Perform a factory reset.
 */
void bluefruit_FactoryReset(void);

/**
 * Force the module into device firmware update mode
 */
void bluefruit_DFU(void);

/**
 * Set the Generic Access Profile advertising data
 * @param data -- the custom advertising data as a byte array string
 */
void bluefruit_GAPSetAdvData(char * data);

/**
 * Clear all custom GATT services and characteristics
 */
void bluefruit_GATTClear(void);

/**
 * Add a new 16 bit GATT service
 * @param uuid -- the custom 16 bit GATT service as a hex string
 */
void bluefruit_GATTAddService(char * uuid);

/**
 * Add a new 128 bit GATT service
 * @param uuid -- the custom 128 bit GATT service as a byte array string
 */
void bluefruit_GATTAddService128(char * uuid);

/**
 * Add a new 16 bit GATT characteristic
 * @param uuid -- the custom 16 bit GATT characteristic as a hex string
 * @param properties -- the 8-bit characteristic properties defined by the Bluetooth SIG
 * @param min_length -- the minimum size of the values for this characteristic
 * @param max_length -- the maximum size of the values for this characteristic
 * @param value -- the initial value to assign to this characteristic
 */
void bluefruit_GATTAddChar(char * uuid, char * properties, uint16_t min_length, uint16_t max_length, char * value);

/**
 * Add a new 128 bit GATT characteristic
 * @param uuid -- the custom 128 bit GATT characteristic as a byte array string
 * @param properties -- the 8-bit characteristic properties defined by the Bluetooth SIG
 * @param min_length -- the minimum size of the values for this characteristic
 * @param max_length -- the maximum size of the values for this characteristic
 * @param value -- the initial value to assign to this characteristic
 */
void bluefruit_GATTAddChar128(char * uuid, char * properties, uint16_t min_length, uint16_t max_length, char * value);

/**
 * Write data to a GATT characteristic
 * @param id -- the id of the GATT characteristic
 * @param data -- the value to assign to the characteristic
 */
void bluefruit_GATTWriteChar(uint8_t id, char * data);

#ifdef	__cplusplus
}
#endif
/** @} */
#endif	/* BLUEFRUIT_H */

