/*
 * vl6180.h
 *
 *  Created on: Mar 11, 2015
 *      Author: Anthony
 *
 *  Adapted from code found @ https://github.com/sparkfun/ToF_Range_Finder-VL6180_Library/
 *  by Casey Kuhns @ SparkFun Electronics
 */

#ifndef VL6180_H_
#define VL6180_H_

#include <stdint.h>

typedef struct vl6180_t {
	uint8_t address;
	uint8_t distance;
	struct settings{
		uint8_t continuous: 1;
	} settings;
} vl6180_t;

#define VL6180_DEFAULT_ADDRESS 0x29

#define VL6180_FAILURE_RESET  -1

#define VL6180_RANGE_CONTINUOUS 1
#define VL6180_RANGE_SINGLE 0

#define VL6180_IDENTIFICATION_MODEL_ID              0x0000
#define VL6180_IDENTIFICATION_MODEL_REV_MAJOR       0x0001
#define VL6180_IDENTIFICATION_MODEL_REV_MINOR       0x0002
#define VL6180_IDENTIFICATION_MODULE_REV_MAJOR      0x0003
#define VL6180_IDENTIFICATION_MODULE_REV_MINOR      0x0004
#define VL6180_IDENTIFICATION_DATE                  0x0006 //16bit value
#define VL6180_IDENTIFICATION_TIME                  0x0008 //16bit value

#define VL6180_SYSTEM_MODE_GPIO0                    0x0010
#define VL6180_SYSTEM_MODE_GPIO1                    0x0011
#define VL6180_SYSTEM_HISTORY_CTRL                  0x0012
#define VL6180_SYSTEM_INTERRUPT_CONFIG_GPIO         0x0014
#define VL6180_SYSTEM_INTERRUPT_CLEAR               0x0015
#define VL6180_SYSTEM_FRESH_OUT_OF_RESET            0x0016
#define VL6180_SYSTEM_GROUPED_PARAMETER_HOLD        0x0017

#define VL6180_SYSRANGE_START                       0x0018
#define VL6180_SYSRANGE_THRESH_HIGH                 0x0019
#define VL6180_SYSRANGE_THRESH_LOW                  0x001A
#define VL6180_SYSRANGE_INTERMEASUREMENT_PERIOD     0x001B
#define VL6180_SYSRANGE_MAX_CONVERGENCE_TIME        0x001C
#define VL6180_SYSRANGE_CROSSTALK_COMPENSATION_RATE 0x001E
#define VL6180_SYSRANGE_CROSSTALK_VALID_HEIGHT      0x0021
#define VL6180_SYSRANGE_EARLY_CONVERGENCE_ESTIMATE  0x0022
#define VL6180_SYSRANGE_PART_TO_PART_RANGE_OFFSET   0x0024
#define VL6180_SYSRANGE_RANGE_IGNORE_VALID_HEIGHT   0x0025
#define VL6180_SYSRANGE_RANGE_IGNORE_THRESHOLD      0x0026
#define VL6180_SYSRANGE_MAX_AMBIENT_LEVEL_MULT      0x002C
#define VL6180_SYSRANGE_RANGE_CHECK_ENABLES         0x002D
#define VL6180_SYSRANGE_VHV_RECALIBRATE             0x002E
#define VL6180_SYSRANGE_VHV_REPEAT_RATE             0x0031

#define VL6180_SYSALS_START                         0x0038
#define VL6180_SYSALS_THRESH_HIGH                   0x003A
#define VL6180_SYSALS_THRESH_LOW                    0x003C
#define VL6180_SYSALS_INTERMEASUREMENT_PERIOD       0x003E
#define VL6180_SYSALS_ANALOGUE_GAIN                 0x003F
#define VL6180_SYSALS_INTEGRATION_PERIOD            0x0040

#define VL6180_RESULT_RANGE_STATUS                  0x004D
#define VL6180_RESULT_ALS_STATUS                    0x004E
#define VL6180_RESULT_INTERRUPT_STATUS_GPIO         0x004F
#define VL6180_RESULT_ALS_VAL                       0x0050
#define VL6180_RESULT_HISTORY_BUFFER                0x0052
#define VL6180_RESULT_RANGE_VAL                     0x0062
#define VL6180_RESULT_RANGE_RAW                     0x0064
#define VL6180_RESULT_RANGE_RETURN_RATE             0x0066
#define VL6180_RESULT_RANGE_REFERENCE_RATE          0x0068
#define VL6180_RESULT_RANGE_RETURN_SIGNAL_COUNT     0x006C
#define VL6180_RESULT_RANGE_REFERENCE_SIGNAL_COUNT  0x0070
#define VL6180_RESULT_RANGE_RETURN_AMB_COUNT        0x0074
#define VL6180_RESULT_RANGE_REFERENCE_AMB_COUNT     0x0078
#define VL6180_RESULT_RANGE_RETURN_CONV_TIME        0x007C
#define VL6180_RESULT_RANGE_REFERENCE_CONV_TIME     0x0080

#define VL6180_READOUT_AVERAGING_SAMPLE_PERIOD      0x010A
#define VL6180_FIRMWARE_BOOTUP                      0x0119
#define VL6180_FIRMWARE_RESULT_SCALER               0x0120
#define VL6180_I2C_SLAVE_DEVICE_ADDRESS             0x0212
#define VL6180_INTERLEAVED_MODE_ENABLE              0x02A3

enum VL6180_als_gain { //Data sheet shows gain values as binary list
	GAIN_20 = 0, // Actual ALS Gain of 20
	GAIN_10,     // Actual ALS Gain of 10.32
	GAIN_5,      // Actual ALS Gain of 5.21
	GAIN_2_5,    // Actual ALS Gain of 2.60
	GAIN_1_67,   // Actual ALS Gain of 1.72
	GAIN_1_25,   // Actual ALS Gain of 1.28
	GAIN_1 ,     // Actual ALS Gain of 1.01
	GAIN_40,     // Actual ALS Gain of 40
};

/**
 * @brief Initializes the VL6180 with required steps
 *
 * Initializes the vl6180_handle to the default address (0x29).
 * Then completes the I2C transactions required by the datasheet:
 * http://www.st.com/st-web-ui/static/active/en/resource/technical/document/application_note/DM00122600.pdf
 *
 * @warning This function should only be called once for each sensor.  Calling this function again may produce
 * errors with communicating with the sensor
 *
 * @param vl6180_handle - The handle to the VL6180 sensor to be initialized
 * @return - Pass = 0, Fail = VL6180_FAILURE_RESET (-1)
 */
int8_t VL6180_Init(vl6180_t* vl6180_handle);

void VL6180_DefautSettings(vl6180_t* vl6180_handle);
void VL6180_StartContinuous(vl6180_t* vl6180_handle);
void VL6180_StopContinuous(vl6180_t* vl6180_handle);
void VL6180_SetRangePeriod(vl6180_t* vl6180_handle, uint8_t period_x10ms);
void VL6180_GetDistance(vl6180_t* vl6180_handle);
uint8_t VL6180_SetAddress(vl6180_t* vl6180_handle, uint8_t address);
uint8_t VL6180_GetRegister(vl6180_t* vl6180_handle, uint16_t regAddr);
uint16_t VL6180_GetRegister16bit(vl6180_t* vl6180_handle, uint16_t regAddr);
void VL6180_SetRegister(vl6180_t* vl6180_handle, uint16_t regAddr, uint8_t data);
void VL6180_SetRegister16bit(vl6180_t* vl6180_handle, uint16_t regAddr, uint16_t data);

void VL6180_StartLog(vl6180_t* vl6180_handle, uint16_t period);
void VL6180_Log(vl6180_t* vl6180_handle);
void VL6180_StopLog(vl6180_t* vl6180_handle);

#endif /* VL6180_H_ */
