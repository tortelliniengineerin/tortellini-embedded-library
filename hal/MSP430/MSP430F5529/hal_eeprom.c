#include "hal_eeprom.h"

unsigned char   DataEEInit          (void);
unsigned int    DataEERead          (unsigned int addr);
unsigned char   DataEEWrite         (unsigned int data, unsigned int addr);

uint16_t EEPROM_data[EEPROM_SIZE];

void hal_EEPROM_Init(void) {
}

uint16_t hal_EEPROM_Read(uint16_t address) {
	if (address > EEPROM_SIZE) return 0;
    return EEPROM_data[address];
}

void hal_EEPROM_Write(uint16_t address, uint16_t data) {
	if (address > EEPROM_SIZE) return;
	EEPROM_data[address] = data;
}
