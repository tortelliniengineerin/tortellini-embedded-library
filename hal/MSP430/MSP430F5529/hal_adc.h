/*
 * adc.h
 *
 *  Created on: Apr 9, 2015
 *      Author: Josh
 */

#include <stdint.h>

#ifndef HAL_ADC_H_
#define HAL_ADC_H_

/**
 * Initializes the ADC system
 */
void hal_adc_init(void);
/**
 * Checks whether the ADC is busy or is able to be polled
 * @return 0 if the ADC is busy
 */
char hal_adc_can_request(void);
/**
 * Gets the last read value of the ADC asynchronously
 * @return ADC value from last successful polling
 */
uint16_t hal_adc_read(void);
/**
 * Waits for the system to get the newest ADC value
 * You probably shouldn't use this.
 * @return Current ADC value
 */
uint16_t hal_adc_read_blocking(void);
/**
 * If the ADC is busy, nothing happens, but if it is available the value is pulled
 * This should be run in a system tick so that the newest value is available as often as possible
 */
void hal_adc_request_if_possible(void);

/**
 * As values are read asynchronously, it's possible that you'll read the same value twice.
 * In order to check every value you pull is a new value, you can use this function.
 *
 * Every the system polls the ADC, a new id is generated. Whenever you read a value, the
 * read id is caught up. When there's discrepancy between the two, this function returns the
 * difference. If the two ids are the same, this function returns 0
 *
 * @return the difference between the poll event and the read event
 */
char hal_adc_new_available(void);

#endif /* ADC_H_ */
