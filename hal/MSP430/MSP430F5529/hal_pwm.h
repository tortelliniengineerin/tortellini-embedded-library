/*
 * hal_pwm.h
 *
 *  Created on: Apr 15, 2015
 *      Author: Josh
 */

#ifndef HAL_PWM_H_
#define HAL_PWM_H_

#include <stdint.h>

void hal_pwm_init(void);

void hal_pwm_setDuty(uint16_t duty);

void hal_pwm_setPeriod(uint16_t period);

void SetVcoreUp(unsigned int level);

void setClock16Mhz(void);

#endif /* HAL_PWM_H_ */
