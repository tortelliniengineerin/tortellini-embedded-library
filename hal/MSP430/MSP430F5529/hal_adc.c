/*
 * adc.c
 *
 *  Created on: Apr 9, 2015
 *      Author: Josh
 */

#include <msp430.h>
#include <stdint.h>
#include "hal_adc.h"

//The last polled value of the ADC
volatile uint16_t last_adc_value;
//The id of the last ADC value you read
volatile char last_read_id;
//The id of the last ADC value that was polled
volatile char last_polled_read_id;

void hal_adc_init(void) {
	ADC12CTL0 = ADC12ON;         // Sampling time, ADC12 on
	ADC12CTL1 = ADC12SHP + ADC12SSEL_3; // Use sampling timer (NEED this), SMCLK
	// ADC12CTL2 = ADC12RES_0; // 8-bit res
	//ADC12IE = BIT0;                           // Enable interrupt
	ADC12CTL0 |= ADC12ENC;
	P6SEL |= BIT0;                            // P6.0 ADC option select
}
char hal_adc_can_request(void) {
	return !(ADC12CTL1 & ADC12BUSY);
}

uint16_t hal_adc_read_blocking(void) {
	ADC12CTL0 |= ADC12SC;
	while (!hal_adc_can_request())  //WTF?!
		;
	//ADC12CTL1 &= ~ADC12IFG0;
	last_read_id++;
	last_adc_value = ADC12MEM0;
	return last_adc_value;
}

uint16_t hal_adc_read(void) {
	last_polled_read_id = last_read_id;
	return last_adc_value;
}

void hal_adc_request_if_possible(void) {
	if(hal_adc_can_request()) {
		hal_adc_read_blocking(); //If there's no problem, get the latest value
	} else {
		ADC12CTL0 |= ADC12SC; //This may do something
		//We can't do anything else
	}
}
char hal_adc_new_available(void) {
	return last_read_id - last_polled_read_id;
}
