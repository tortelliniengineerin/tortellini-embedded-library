#include "system.h"
#include "hal_keyfob_receiver.h"


#define Set_IC1_PPS(pin) RPINR7bits.IC1R = pin

void hal_Keyfob_Init(void) {
	// Init digital interrupt
    P1IE |= BIT6 ; // Interrupt on Input Pin P1.6
    P1IES |= BIT6; //  High to Low Edge

    // Init timer 1
    TA1CTL = TASSEL_2 + MC_2 + TACLR;         // SMCLK, contmode, clear TAR	//
}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=PORT1_VECTOR
__interrupt void PORT1_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(PORT1_VECTOR))) PORT1_ISR (void)
#else
#error Compiler not supported!
#endif
{
	static uint16_t prev_count = 0;    // I assume that this runs only the first time that this
	Keyfob_ProcessPulse(TA1R-prev_count);
	prev_count = TA1R;

	P1OUT ^= BIT0;
	P1IES ^= BIT6 ; // Toggle Edge sensitivity
	P1IFG &= ~BIT6; // Clear Interrupt Flag
}
